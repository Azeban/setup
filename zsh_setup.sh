#!/bin/bash

# Uncomment if running only this
# sudo apt update && sudo apt upgrade -y 

echo -e "\n\n\n### Installing Zsh ###\n"

sudo apt install -y zsh

sudo usermod -s /usr/bin/zsh $(whoami)

# currently set on different lines since I dont yet know if it affect dependencies
sudo apt install -y powerline fonts-powerline zsh-theme-powerlevel9k zsh-syntax-highlighting guake
# sudo apt install -y  zsh-theme-powerlevel9k
echo "source /usr/share/powerlevel9k/powerlevel9k.zsh-theme" >> ~/.zshrc
# sudo apt install -y zsh-syntax-highlighting guake
echo "source /usr/share/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh" >> ~/.zshrc

# --unnattended allows entire script to finish before switching to zsh
sh -c "$(curl -fsSL https://raw.githubusercontent.com/ohmyzsh/ohmyzsh/master/tools/install.sh)" "" --unattended 

git clone https://github.com/ohmyzsh/ohmyzsh.git ~/.oh-my-zsh

mv ~/Documents/setup/.zshrc ~
