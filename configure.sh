#!/bin/bash

# usage: log(msg)
function log() {
	date=$(date +"%F %T")
	echo "[*] ${date} ${1}"
}

export DEBIAN_FRONTEND=noninteractive

log "Enabling 32-bit packages"
sudo dpkg --add-architecture i386

log "Updating system packages"
sudo -E apt update && sudo apt upgrade -y

# --install-suggests adds like an hour to install, no using for now
log "Installing new packages via apt"
sudo -E apt-get install -y python3 python3-pip wireshark ruby gdb curl forensics-extra net-tools libc6:i386 libncurses5:i386 libstdc++6:i386 lib32z1

log "Upgrading pip"
pip3 install --upgrade pip

log "Creating virtualenv"
python3 -m pip install  virtualenv 
python3 -m virtualenv ~/pyenv
source ~/pyenv/bin/activate

log "Installing pwntools"
# install python3 tools
python3 -m pip install pwntools

log "Installing pip packages"
python3 -m pip install pillow scapy

# because python dependences are a pain in the ass
# Also adds to path
log "Adding venv to .bashrc"
if [[ -f ~/.zshrc ]]; then
        echo "source ~/pyenv/bin/activate" >> ~/.zshrc
	echo "alias gdb=\"gdb -q\"" >> ~/.zshrc
else
        echo "source ~/pyenv/bin/activate" >> ~/.bashrc
	echo "alias gdb=\"gdb -q\"" >> ~/.bashrc
fi

deactivate # leave virtualenv so pwndbg doesn't break

log "Installing gem packages"
sudo gem install one_gadget

log "Configuring gdb"
git clone https://github.com/pwndbg/pwndbg ~/.pwndbg
cd ~/.pwndbg
chmod +x setup.sh
./setup.sh

cd ~

log $'\n'"Configuration finished, please reboot your VM"
log $'\n'"P.S. HONK THE PLANET!"

