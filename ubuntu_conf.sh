#!/bin/bash

echo -e "\n\n\n### Making themes and icons in home ###\n"
mkdir ~/.themes
mkdir ~/.icons

sudo apt install -y gnome-tweaks gnome-shell-extensions

echo -e "\n\n\n### Setting up themes ###\n"
# Themes
cd ~/Documents

# Nordic
git clone https://github.com/eliverlara/nordic
cp -r ~/Documents/nordic ~/.themes

gsettings set org.gnome.desktop.interface gtk-theme "Nordic"
gsettings set org.gnome.desktop.wm.preferences theme "Nordic"

# Dracula
git clone https://github.com/EliverLara/Ant-Dracula
cp -r ~/Documents/Ant-Dracula ~/.themes

gsettings set org.gnome.desktop.interface gtk-theme "Ant-Dracula"
gsettings set org.gnome.desktop.wm.preferences theme "Ant-Dracula"

# Clean up Documents
echo -e "\n\n\n### Cleaning up theme repos ###\n"
rm -rf ~/Documents/nordic
rm -rf ~/Documents/Ant-Dracula

echo -e "\n\n\n### fixing some of ubuntu's dumbness ###\n"
# Removed the dumb dock shortcuts
gsettings set org.gnome.shell.extensions.dash-to-dock hot-keys false

# Sets it so that clicking on launcher minimizes if already open
gsettings set org.gnome.shell.extensions.dash-to-dock click-action 'minimize'

cd ~
