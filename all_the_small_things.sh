#!/bin/bash

sudo apt update && sudo apt upgrade -y

./progs.sh

echo -e "\n\n\n#Zsh setup\n" 

./zsh_setup.sh

echo -e "\n\n\n#ubuntu setup\n"

./ubuntu_conf.sh

echo -e "\n\n\n#configure setup\n"

./configure.sh

echo -e "\n\n\nYou can reboot now\n"

