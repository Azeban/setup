#!/bin/bash

sudo apt update && sudo apt upgrade -y 

# include cutter later
sudo apt install -y tilix fonts-roboto vim htop

# makes directory if needed
mkdir -p ~/.config/tilix/schemes 
cp ~/Documents/setup/icy-hot.json ~/.config/tilix/schemes

# fixing issues with tilix 
#echo -e "if [ \$TILIX_ID ] || [ \$VTE_VERSION ]; then 
#       source /etc/profile.d/vte.sh
#fi" >> ~/.zshrc


# SearchSploit
#git clone https://github.com/offensive-security/exploitdb.git /opt/exploitdb

#ln -sf /opt/exploitdb/searchsploit /usr/local/bin/searchsploit

#cp -n /opt/exploitdb/.searchsploit_rc ~/

# Installs Gatsby
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.1/install.sh | zsh
source ~/.zshrc
nvm install 10
nvm use 10
# checks that it's installed
npm --version
node --version
# check deprecation warnings
npm install -g gatsby-cli
gatsby telemetry --disable

# VScodium
# plugins: 'Bracket Pair Colorizer 2', Synthwave '84 theme,
#echo -e "\n\n### Installing Codium ###\n" 

wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg | gpg --dearmor | sudo dd of=/etc/apt/trusted.gpg.d/vscodium.gpg 

echo 'deb https://paulcarroty.gitlab.io/vscodium-deb-rpm-repo/debs/ vscodium main' | sudo tee --append /etc/apt/sources.list.d/vscodium.list 

sudo apt update && sudo apt install codium

